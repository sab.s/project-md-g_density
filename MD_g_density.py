import MDAnalysis as mda
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", required=True, help="input file (gro)")
parser.add_argument("-t", required=True, help="trajectory file (xtc)")
parser.add_argument("-o", default="out.xvg", help="output file (default=out.xvg)")
parser.add_argument("-sl", default=100, help="number of slides <int> (default=100)")
parser.add_argument("-mol", default=0, help=f"type of molecule <int> (default=0) : "
                                            f"System(0) POPC(1) POPE(2) TIP3(3) POPC+POPE(4)")
args = parser.parse_args()

u = mda.Universe(args.i, args.t)

# find the max and min value of Z
z_max = u.atoms.positions[0][2]
z_min = u.atoms.positions[0][2]
for position in u.atoms.positions:
    if position[2] < z_min:
        z_min = position[2]
    if position[2] > z_max:
        z_max = position[2]

# divide Z into n slide
n_slide = args.sl
z_slide = (z_max - z_min) / n_slide

# calculate the volume :
# volume of a slide = X * Y * Z/100
v_slide = u.dimensions[0] * u.dimensions[1] * z_slide

# initialize list of density average
density_mean = [0] * n_slide


# get the molecule type in string from user input
def switch_type(mol_type=0):
    switcher = {
        0: "System",
        1: "POPC",
        2: "POPE",
        3: "TIP3",
        4: "POPC + POPE"
    }
    return switcher[mol_type]


def calculate_mass(atoms):
    # 1. create a list with n elements and initialize it with 0
    mass_slide_list = [0] * n_slide
    # 2. loop down the atoms and attribute its mass to the corresponding slide
    for atom in atoms:
        slide_number = int((atom.position[2] - z_min) / z_slide)
        # the atom at max Z position belongs to the lower slide
        if slide_number == n_slide:
            slide_number = n_slide - 1
        mass_slide_list[slide_number] += atom.mass
    return mass_slide_list


def calculate_density(mol_type):
    # select atoms
    if mol_type != 0:
        atoms = u.select_atoms(f'resname {switch_type(mol_type)}')
    else:
        atoms = u.atoms

    # calculate the average densities
    for trj in u.trajectory:
        # calculate the mass for each slide
        mass_slide_list = calculate_mass(atoms)

        # accumulate the density of each frame :
        for i, mass in enumerate(mass_slide_list):
            density = mass / v_slide
            density = density * 1.66053906660e-27 / 1.0e-30  # convert to kg/m3
            density_mean[i] += density

calculate_density(int(args.mol))


# locate the minima through which the axis of symmetry of the graphic passes
min_density = min(density_mean)
index_min = density_mean.index(min_density)

# divide the list into two parts and compute the max values  of each part
max_1 = max(density_mean[0: index_min])
index_max1 = density_mean.index(max_1)
max_2 = max(density_mean[index_min:])
index_max2 = density_mean.index(max_2)


calculate_density(int(args.mol))

with open(args.o, "w") as f:
    # header of .xvg file :
    f.write(f'@    title "{"System density" if int(args.mol) == 0 else "Partial density"}"\n'
            f'@    xaxis  label "Coordinate (angstrom)"\n'
            f'@    yaxis  label "Density (kg m\\S-3\\N)"\n'
            f'@TYPE xy\n'
            f'@ view 0.15, 0.15, 0.75, 0.85\n'
            f'@ legend on\n'
            f'@ legend box on\n'
            f'@ legend loctype view`n'
            f'@ legend 0.78, 0.8\n'
            f'@ legend length 2\n'
            f'@ s0 legend "{switch_type(int(args.mol))}"')
    # write the .xvg file :
    # center of slice (z in col 1) and average density (density in col 2)
    for i, density in enumerate(density_mean):
        z = (i+i+1)/2 * z_slide
        density /= len(u.trajectory)
        f.write(f"{z:8.3f}\t{density:8.3f}\n")
        # find the z of the two max values
        if i == index_max1:
            z_max1 = i
        if i == index_max2:
            z_max2 = i

# compute the thickness of the membrane
# membrane_size = z_max2 - z_max1
# print("l'epaisseur de la membrane est de : " + str(membrane_size) + "A")
