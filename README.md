MD_g_density
=============================================================================

MD_g_density is a python program for computing partial densities of cell membrane across the box. Some of its features include:

- Customized for cell membrane molecular structure file (.gro) 
and trajectory file (.xtc) containing POPE, POPC and TIP3 atoms.
- Generate a .xvg file which can be displayed in graphical form 
with GRACE program on LINUX systems. 
- Possible to select the atom type to calculate density.


Development environment                                      
-----------------------------------------------------------------------------
MD_g_density was developped by python 3.8.8 using MDAnalysis 
package and argparse module. 

From Python 2.7 and 3.2, the argparse module is maintained in 
the standard Python library. For users who still need to support
 lower version, it is also provided as a separate package. You 
 can find more information on [argparse 1.4.0](https://pypi.org/project/argparse/).

For more information on MDAnalysis installation please visit the
 [MDAnalysis Installation Quick Start](https://www.mdanalysis.org/pages/installation_quick_start/).


Usage
-----------------------------------------------------------------------------
- required arguments:  
  -i I        input file (gro)  
  -t T        trajectory file (xtc)  

- optional arguments:  
  -h, --help  show this help message and exit  
  -o O        output file (default=out.xvg)  
  -sl SL      number of slides <int> (default=100)  
  -mol MOL    type of molecule <int> (default=0) : System(0) POPC(1) POPE(2) TIP3(3)  

run in terminal (example):
```
python MD_g_density.py -i start_frame.gro -t trajectory.xtc 
```

Authors
-----------------------------------------------------------------------------
Sabrina SAFAR-REMALI
Yanyuan ZHANG


GitLab
-----------------------------------------------------------------------------
You can find our source code repository and demo files on GitLab 
at [https://gitlab.com/sab.s/project-md-g_density](https://gitlab.com/sab.s/project-md-g_density). 
